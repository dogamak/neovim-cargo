syn include @RUST syntax/rust.vim

if !exists('g:cargo_syntax_highlight_errors')
	let g:cargo_syntax_highlight_errors=1
endif

if g:cargo_syntax_highlight_errors
	syn region cargoErrorCode start=/^[ 0-9]\+|/ms=e end=/$/ contains=@RUST
endif

if !exists('g:cargo_syntax_highlight_explanations')
	let g:cargo_syntax_highlight_explanations=1
endif

if g:cargo_syntax_highlight_explanations
	syn region cargoExplanationCodeBlock start=/```.*$/ms=e end=/```/ contains=@RUST matchgroup=cargoExplanationCodeBlockBackticks
endif

hi cargoLineNumbers ctermfg=7
hi cargoSpanIndicator ctermfg=9
hi cargoError ctermfg=9
hi cargoWarning ctermfg=11

hi Folded ctermbg=NONE
