syn match cargoInfo /^\s\+\(Finished\|Running\|Doc-tests\)/
syn match cargoTestCountLine /^running [0-9]\+ tests\?/ contains=cargoTestCountNumber
syn match cargoTestCountNumber /[0-9]\+/ contained

syn match cargoTestResultLine /^test .*\.\.\..*$/ contains=cargoTestResult,cargoTestResultTestName
syn keyword cargoTestResult ok fail contained
syn match cargoTestRsultTestName /^test [^ ]\+/hs=s+5 contained

hi cargoInfo ctermfg=2 cterm=bold
hi cargoTestCountLine ctermfg=8
hi cargoTestCountNumber ctermfg=7
