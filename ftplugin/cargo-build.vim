set foldmethod=manual
"set foldexpr=CargoBuildFoldlevel(v:lnum)
set buftype=nofile
set nowrap

function CargoBuildFoldText()
	set virtualedit=all
	norm! g$
	let s:width = virtcol('.')
	set virtualedit=

	return repeat('-', s:width - 14) . '[EXPLANATION]-'
endfunction

set foldtext=CargoBuildFoldText()
