import neovim
import subprocess
import json
import os
import re
from cargo.advanced_buffer import AdvancedBuffer

class Settings(object):
    def __init__(self, vim):
        self._eval = vim.eval
        self.variables = ['color', 'explanations']
    
    def __getattr__(self, name):
        if name not in self.variables:
            raise AttributeError
        return self._eval('g:cargo_'+name)

@neovim.plugin
class Cargo(object):
    BUILD = 0
    TEST = 1
    RUN = 3

    def __init__(self, vim):
        self.vim = vim
        self.settings = Settings(vim)
        self.project_buffers = {}
        self.out_buffers = {}
        self.projects = {}
        self.create_log_buffer()

    def create_log_buffer(self):
        self.vim.command('new')
        self.log_buffer = self.vim.current.buffer
        self.vim.command('wincmd c')

    def call_cargo(self, *args, path=None, manifest=None):
        if manifest is None and path is None:
            manifest = self.get_project_info()['manifest_path']

        if path is None:
            path = os.path.dirname(manifest)


        process = subprocess.Popen(["/bin/sh", "-c", "cargo "+' '.join(args)+'|tee /tmp/neovim-cargo-tee'], cwd=path, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        return process

    def get_project_info(self):
        manifest = None
        bufnr = self.vim.current.buffer.number
        if bufnr in self.project_buffers:
            manifest = self.project_buffers[bufnr]
        else:
            manifest = self.get_manifest_path()
            self.project_buffers[bufnr] = manifest

        project = None
        if manifest in self.projects:
            project = self.projects[manifest]
        else:
            project = self.read_project_info(manifest)
            self.projects[manifest] = project

        return project

    def read_project_info(self, manifest):
        process = self.call_cargo("read-manifest", manifest=manifest)
        info = process.communicate()[0]
        return json.loads(info)

    def get_manifest_path(self):
        current_path = self.vim.call('expand', '%:p:h')
        process = self.call_cargo('locate-project', path=current_path)
        output = process.communicate()
        return json.loads(output[0])['root']

    def get_buffer(self, operation, keep_split=False):
        manifest = self.get_manifest_path()
        buffer_nr = None
        if manifest in self.out_buffers:
            if operation in self.out_buffers[manifest]:
                buffer_nr = self.out_buffers[manifest][operation]
            else:
                buffer_nr = self.create_buffer(operation, keep_split)
                self.out_buffers[manifest][operation] = buffer_nr
        else:
            buffer_nr = self.create_buffer(operation, keep_split)
            self.out_buffers[manifest] = { operation: buffer_nr }
        return self.vim.buffers[buffer_nr]

    def get_buffer_and_show(self, operation):
        buf = self.get_buffer(operation, True)
        win = self.show_buffer(buf)
        return (buf, win)

    def create_buffer(self, operation, keep_split=False):
        info = self.get_project_info()
        buffer_name = "Cargo {}: {}".format(operation, info['name'])
        self.vim.command("rightbelow vnew")
        self.vim.current.buffer.name = buffer_name
        self.vim.command("set filetype=cargo-{}".format(operation))
        bufnr = self.vim.current.buffer.number
        if not keep_split:
            self.vim.command('wincmd c')
        return bufnr

    def show_buffer(self, buffer):
        for window in self.vim.current.tabpage.windows:
            if window.buffer.number == buffer.number:
                return window
        self.vim.command('rightbelow vsplit')
        self.vim.command("buffer {}".format(buffer.number))
        window = self.vim.current.window
        return window

    @neovim.command('CargoClean')
    def cmd_cargo_clean(self):
        self.call_cargo('clean')

    def append_highlight(self, buffer, group, text):
        line = len(buffer)
        column = len(buffer[line])
        buffer[line]

    def add_quickfix_item(self, msg):
        info = {}

        info['type'] = msg['level'][0].upper()
        info['text'] = msg['message']

        if 'spans' in msg and len(msg['spans']) > 0:
            info['filename'] = msg['spans'][0]['file_name']
            info['lnum'] = msg['spans'][0]['line_start']
            info['col'] = msg['spans'][0]['column_start']

        self.vim.call('setqflist', [info], 'a')

    @neovim.command('CargoBuild', bang=True)
    def cmd_cargo_build(self, bang):
        if bang:
            self.cmd_cargo_clean()
        (buf, win) = self.get_buffer_and_show('build')
        buf[:] = [""]
        process = self.call_cargo('build', '--message-format=json')
        hibuf = AdvancedBuffer(self.vim, buf)

        while True:
            line = process.stdout.readline()
            if not line: break
            line = line[:-1].decode('utf-8')
            if line.startswith('error: Could not compile'): break
            if len(line) == 0 or line[0] != '{': continue

            error = json.loads(line)
            if error['reason'] == 'compiler-message':
                msg = error['message']
                self.add_quickfix_item(msg)
                self.write_compiler_message(hibuf, msg)
            hibuf.flush()

    def write_compiler_message(self, hibuf, msg):
        def write_primary_span():
            if 'spans' not in msg or len(msg['spans']) == 0:
                return

            span = next((span for span in msg['spans'] if span['is_primary']), msg['spans'][0])
            hibuf.append_list([
                ['  '],
                ['cargoErrorSpanArrow', '-->'],
                [' '],
                ['cargoErrorFile', span['file_name']],
                [':'],
                ['cargoErrorLine', str(span['line_start'])],
                [':'],
                ['cargoErrorColumn', str(span['column_start'])],
                ['\n']])

        if msg['level'] == 'error':
            hibuf.append('error', 'cargoError')
            if msg['code']:
                hibuf.append('[')
                hibuf.append(msg['code']['code'], 'cargoErrorCode')
                hibuf.append(']: {}\n'.format(msg['message']))
                write_primary_span()
                if self.settings.explanations:
                    hibuf.begin_fold()
                    hibuf.append(msg['code']['explanation'])
                    hibuf.end_fold()
            else:
                hibuf.append(': {}\n'.format(msg['message']))
                write_primary_span()
        elif msg['level'] == 'warning':
            hibuf.append('warning', 'cargoWarning')
            hibuf.append(': {}\n'.format(msg['message']))
            write_span()
        elif msg['level'] == 'note':
            hibuf.append('    = note: {}\n'.format(msg['message']))
        
        if 'spans' in msg and len(msg['spans']) > 0:
            spans = msg['spans']
            lnum_width = len(str(sorted([ span['line_start'] for span in spans ])[-1]))
            
            def w_lnum_col(lnum='', text='', highlight=None):
                lnum = str(lnum)
                col = ' ' + lnum.rjust(lnum_width) + ' | '
                hibuf.append(col, 'cargoLineNumbers')
                hibuf.append(text + '\n')
                if highlight is not None:
                    hibuf.append(' '*(lnum_width+2)+'|', 'cargoLineNumbers')
                    hibuf.append(' '*(highlight[0]))
                    hibuf.append('^'*(highlight[1]-highlight[0]), 'cargoSpanIndicator')
                    hibuf.append('\n')

            w_lnum_col()
            for span in spans:
                span['text'][0]['line_start'] = span['line_start']
                for line in span["text"]:
                    highlight = None
                    if 'highlight_start' in line:
                        highlight = (line['highlight_start'], line['highlight_end'])
                    w_lnum_col(line.get('line_start', ''), line['text'], highlight)
            w_lnum_col()

            children = msg.get('children', [])
            for child in children:
                self.write_compiler_message(hibuf, child)
        hibuf.append('\n')
        #hibuf.end_fold()
    
    def cargo_build_begins_foldlevel(self, line):
        if line.startswith('warning'):
            return '2'
        if line.startswith('error'):
            return '1'
        if line.strip().split(' ', 1)[0] in ['Finished', 'Compiling']:
            return '0'
        return None

    @neovim.function('CargoBuildFoldlevel', sync=True)
    def cargo_build_foldlevel(self, args):
        linenr = args[0] - 1
        buffer = self.vim.current.buffer
        line = buffer[linenr]

        back = 0
        prev_level = None
        while linenr - back >= 0:
            begins = self.cargo_build_begins_foldlevel(buffer[linenr-back])
            if begins is not None:
                prev_level = begins
                break
            back += 1

        if prev_level is None:
            return '0'

        if back is 0:
            return '>' + prev_level

        if line is '':
            if prev_level is '0':
                return '0'
            else:
                return '<' + prev_level

        return prev_level

    @neovim.function('TestHighlight')
    def test_highlight(self, args):
        buf = self.vim.current.buffer
        linenr = 0
        for line in buf:
            try:
                start = line.lower().index('world')
                end = start + len('world')
                buf.add_highlight('GruvboxOrange', linenr, start, end)
            except ValueError:
                pass
            linenr += 1

    def append_color(self, buffer, text):
        buffer.append(color_code_regex.replace(b'', text))
        lines = len(buffer)
        for match in color_code_regex.finditer(text):
            buffer.add_highlight(groups[match.group(1)], line, match.start(), match.end()) 
    
    def scroll_to_end(self, buffer):
        pass

    def pipe_to_buffer(self, buffer, file):
        while True:
            line = file.readline()
            if not line: break
            line = line[:-1].decode('utf-8')
            buffer.append(line)
            self.scroll_to_end(buffer)

    @neovim.command('CargoTest', bang=True, nargs='*')
    def cargo_test(self, args, bang):
        if bang:
            args.extend(['--', '--nocapture'])

        buffer = self.get_buffer('test')
        self.show_buffer(buffer)

        process = self.call_cargo('test', *args)
        self.pipe_to_buffer(buffer, process.stdout)

    @neovim.function('CargoTestFoldlevel', sync=True)
    def cargo_test_foldlevel(self, args):
        buffer = self.vim.current.buffer
        linenr = args[0] - 1
        if buffer[linenr].strip().startswith('Running'):
            return '>1'
        if buffer[linenr-1].startswith('test result:'):
            return '<1'
        return '-1'

    @neovim.function('CargoTestFoldText', sync=True)
    def cargo_test_fold_text(self, args):
        raise Exception(args)
        foldstart = args[0]
        foldend = args[1]
        first = self.vim.current.buffer[foldstart]
        return re.sub(r'^\s+Running .*/\([^/]+\)-[a-z0-9]+$', '     Running \1', first)
