from types import *

class HighlightSection(object):
    def __init__(self, start, end, group):
        self.start = start
        self.end = end
        self.group = group

class HighlightLine(object):
    def __init__(self, line="", group=None):
        self.line = line
        if group is None:
            self.sections = []
        else:
            self.sections = [HighlightSection(0, len(line), group)]

    def append(self, text, group=None):
        text = text.replace('\n', '<CR>')
        self.line += text
        if group is not None:
            end = len(self.line)
            start = end - len(text)
            self.sections.append(HighlightSection(start, end, group))

    def write(self, buf):
        buf.append(self.line)
        line = len(buf) - 1
        for section in self.sections:
            buf.add_highlight(section.group, line, section.start, section.end)

class Fold(object):
    def __init__(self, typ):
        self.typ = typ
Fold.Begin = Fold(True)
Fold.End = Fold(False)

class AdvancedBuffer(object):
    def __init__(self, vim, buf):
        self.vim = vim
        self.buf = buf
        self.items = []
        self.fold_starts = []
        self.indents = []

    def begin_fold(self):
        self.items.append(Fold.Begin)

    def end_fold(self):
        self.items.append(Fold.End)

    def set_indent(self, level=None):
        if level is None:
            if isinstance(self.items[-1], HighlightLine):
                level = len(self.items[-1].text)
            else:
                level = 0
        self.indents.append(level)

    def clear_indent(self):
        self.indents.pop()

    def indent(self):
        indent = self.indent[-1]
        if isinstance(indent, IntType):
            return ' '*indent
        elif isinstance(indent, str):
            return indent
        elif isinstance(indent, FunctionType):
            return indent()
        else:
            raise TypeError

    def append(self, text, group=None):
        lines = text.split('\n')
        if len(self.items) > 0 and isinstance(self.items[-1], HighlightLine):
            self.items[-1].append(lines[0], group)
            del lines[0]
        for line in lines:
            self.items.append(HighlightLine(' '*self.indent() + line, group))

    def append_list(self, list):
        for item in list:
            if len(item) is 1:
                item.insert(0, None)
            self.append(item[1], item[0])
    
    def flush(self):
        fold_begins = []
        for item in self.items:
            if isinstance(item, HighlightLine):
                item.write(self.buf)
            elif isinstance(item, Fold):
                if item is Fold.Begin:
                    fold_begins.append(len(self.buf))
                else:
                    begin = fold_begins.pop()
                    end = len(self.buf)
                    focused = self.vim.current.window.number
                    self.vim.command('{}wincmd w'.format(self.buf.window.number))
                    self.vim.command(':{},{}fold'.format(begin, end))
                    self.vim.command('{}wincmd w'.format(focused))
        self.item = []
